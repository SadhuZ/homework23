#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;


int Sum(int *bill, int size){
    int sum = 0;
    for(int i = 0; i < size; i++)
        sum += bill[i];
    return sum;
}

int main()
{
    const int billsNum = 1000;
    int bill[1000];
    ifstream fileATM;
    fileATM.open("ATM.txt");
    if(!fileATM.is_open()){
        cout << "ATM is empty." << endl;
        for(int i = 0; i < billsNum; i++)
            bill[i] = 0;
    }
    else{
        for(int i = 0; i < billsNum; i++)
            fileATM >> bill[i];
    }
    fileATM.close();

    char operation;
    cout << "Enter operation (+/-): ";
    cin >> operation;
    if(operation == '+'){
        ofstream fileATM;
        fileATM.open("ATM.txt");
        int bills[] = {100, 200, 500, 1000, 2000, 5000};
        for(int i = 0; i < billsNum; i++){
            if(bill[i] == 0){
                bill[i] = bills[rand() / (RAND_MAX / 6)];
            }
            fileATM << bill[i] << endl;
        }
        fileATM.close();
    }
    else if(operation == '-'){
        int sum;
        cout << "Enter a sum to withdrawal: ";
        cin >> sum;
        if(sum > Sum(bill, billsNum)){
            cout << "Not enough money." << endl;
        }
        else{
            sort(bill, bill + billsNum, std::greater<int>());
            for(int i = 0; i < billsNum; i++){
                if(sum - bill[i] >= 0){
                    sum -= bill[i];
                    bill[i] = 0;
                }
                if(sum == 0){
                    break;
                }
            }
            if(sum == 0){
                cout << "Take the money." << endl;
                ofstream fileATM;
                fileATM.open("ATM.txt");
                for(int i = 0; i < billsNum; i++){
                    fileATM << bill[i] << endl;
                }
                fileATM.close();
            }
            else{
                cout << "Not enough bills." << endl;
            }
        }
    }
    else{
        cout << "Wrong operation.";
    }
    return 0;
}
