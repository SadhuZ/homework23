#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ifstream fRiver;
    fRiver.open("river.txt");
    if(!fRiver.is_open()){
        cout << "File river.txt open error." << endl;
        return -1;
    }
    string fishName;
    cout << "Enter a fish name: ";
    cin >> fishName;
    ofstream fBasket;
    fBasket.open("basket.txt", ios::app);
    while (!fRiver.eof()) {
        string fish;
        fRiver >> fish;
        if(fish == fishName)
            fBasket << fish << endl;
    }
    fBasket.close();
    fRiver.close();
    return 0;
}
