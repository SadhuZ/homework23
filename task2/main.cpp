#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    int width;
    int high;
    cout << "Enter width: ";
    cin >> width;
    cout << "Enter high: ";
    cin >> high;

    const char* fileName = "picture.txt";
    ofstream file;
    file.open(fileName);

    srand(time(nullptr));
    for(int h = 0; h < high; h++){
        for(int w = 0; w < width; w++){
            int random_variable = rand() > RAND_MAX / 2;
            file << random_variable;
        }
        file << endl;
    }

    file.close();
    return 0;
}
