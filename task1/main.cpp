#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    const char* fileName = "vedomost.txt";
    ofstream vedomost;
    vedomost.open(fileName, ios::app);
    string name;
    string surename;
    string date;
    float payment;
    cout << "Name: ";
    cin >> name;
    cout << "Surname: ";
    cin >> surename;
    while(1){
        cout << "Date (dd.mm.yyyy): ";
        cin >> date;
        if(stoi(date.substr(0, 2)) >= 1 && stoi(date.substr(0, 2)) <= 31 &&
                stoi(date.substr(3, 2)) >= 1 && stoi(date.substr(3, 2)) <= 12 &&
                stoi(date.substr(7, 4)) >= 1 && stoi(date.substr(7, 4)) <= 9999 &&
                date[2] == '.' && date[5] == '.' && date.size() == 10)
            break;
        cout << "Wrong date!" << endl;
    }
    cout << "Payment: ";
    cin >> payment;
    vedomost.setf(ios::showpoint);
    vedomost.setf(ios::fixed);
    vedomost.precision(2);
    vedomost << name << " " << surename << " " << date << " " << payment << endl;
    vedomost.close();
    return 0;
}
